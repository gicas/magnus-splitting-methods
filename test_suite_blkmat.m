function test_suite = test_of_bm
try % assignment of 'localfunctions' is necessary in Matlab >= 2016
    test_functions = localfunctions();
catch % no problem; early Matlab versions can use initTestSuite fine
end
initTestSuite;
end

% Test 1: make matrix from a scalar
function test_bm_scalar
bm = blkmat;
assertEqual(bm.upper(1, 1), ...
    [[1, 1]; [0, 1]]);
assertEqual(bm.lower(1, 1), ...
    [[1, 0]; [1, 1]]);
end

function test_bm_const_mat
bm = blkmat;
dim = 2;
R = rand(dim);
I = eye(dim);
Z = zeros(dim);
assertEqual(bm.upper(R, 1), ...
    [[I, R]; [Z, I]]);
assertEqual(bm.lower(R, 1), ...
    [[I, Z]; [R, I]]);
end

function test_function_handle
bm = blkmat;
dim = 3;
I = eye(dim);
Z = zeros(dim);
T = @(t) t * rand(dim);
val_T = T(rand);

assertEqual(bm.upper(val_T, 1), ...
    [[I, val_T]; [Z, I]]);
assertEqual(bm.lower(val_T, 1), ...
    [[I, Z]; [val_T, I]]);
end