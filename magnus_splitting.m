function result = magnus_splitting( ...
    F, G, t_span, iv_F, iv_G, n_steps, ...
    coef_F, coef_G, qp_vec)
% MAGNUS_SPLITTING solves a DE system, split into parts F and G.
%   MAGNUS_SPLITTING(F, G, t_span, iv_F, iv_G, steps_amb, coef_F, coef_G, qp_vec)
%   solves a DE system split into F (acts of the 2nd part in this methods) 
%   and G with the initial values IV_F and IV_G (vectors of 'flat' matrices),
%   on T_SPAN, using time steps that are defined by STEPS_AMB.
%   COEF_F and COEF_G are corresponding coefficient matrices or vectors.
%   QP_VEC should contain quadrature points.

if ~isa(F, 'function_handle')
    F = @(t) F;
end
if ~isa(G, 'function_handle')
    G = @(t) G;
end
dim_pr = size(F(0));

t0 = t_span(1);
tf = t_span(end);
h = (tf - t0) / n_steps;

V = iv_F;
W = iv_G;

if size(coef_F, 1) == size(coef_G, 1) + 1
    coef_G = [coef_G; zeros(1, size(coef_G, 2))];
end

n_qp = length(qp_vec);

for step = 1:n_steps
    t = t0 + (step - 1) * h;
    
    Aqp = zeros([dim_pr, n_qp]);
    for idx = 1:n_qp
        Aqp(:,:, idx) = F(t+h*qp_vec(idx));
    end
    
    Bqp = zeros([dim_pr, n_qp]);
    for idx = 1:n_qp
        Bqp(:,:, idx) = G(t+h*qp_vec(idx));
    end
    
    for idx = 1:size(coef_G, 1)
        % Multiply each page of the 3D array by the corresponding coef.
        tmp = permute(repmat(coef_F(idx,:), [dim_pr(1), 1, dim_pr(2)]), [1, 3, 2]);
        Ai = bsxfun(@times, Aqp, tmp);
        Ai = sum(Ai, 3);
        if isvector(coef_F)
            Ai = Ai / n_qp;
        end
        B_tmp = reshape(Bqp, prod(dim_pr), n_qp) .* coef_G(idx,:);
        Bi = sum(reshape(B_tmp, [dim_pr, n_qp]), 3);
        
        V = V + h * Ai * W;
        W = W + h * Bi * V;
    end
end
result = [V; W];

% end of function
end