function test_suite = test_of_ms
try % assignment of 'localfunctions' is necessary in Matlab >= 2016
    test_functions = localfunctions();
catch % no problem; early Matlab versions can use initTestSuite fine
end
initTestSuite;
end

function test_nonaut_splittig_function
bm = blkmat;
a = [[1, 3, 5]; ...
    [7, 11, 13]];
b = [[17, 19, 23]; ...
    [29, 31, 37]];
c = [3, 5, 7];
h = 1.0;
t = 0.0;
t_span = [t, h];
dim = [2, 2];
A = @(t) - [[1, 2]; [3, 4]] * t;
B = @(t) [[1, 2]; [3, 4]] * t;

A1 = A(t+h*c(1));
A2 = A(t+h*c(2));
A3 = A(t+h*c(3));
B1 = B(t+h*c(1));
B2 = B(t+h*c(2));
B3 = B(t+h*c(3));

result = ... 
    bm.lower(b(2, 1)*B1+b(2, 2)*B2+b(2, 3)*B3, h) * ...
    bm.upper(a(2, 1)*A1+a(2, 2)*A2+a(2, 3)*A3, h) * ...
    bm.lower(b(1, 1)*B1+b(1, 2)*B2+b(1, 3)*B3, h) * ...
    bm.upper(a(1, 1)*A1+a(1, 2)*A2+a(1, 3)*A3, h);

assertElementsAlmostEqual(magnus_splitting(A, B, [t, h], [eye(dim), zeros(dim)], [zeros(dim), eye(dim)], 1, a, b, c), result);
end

function test_splitting_const_matrix
bm = blkmat;
dim = 3;
R = rand(dim);
I = eye(dim);
Z = zeros(dim);
a = [[1, 3, 5]; ...
    [7, 11, 13]];
b = [[17, 19, 23]; ...
    [29, 31, 37]];
c = [3, 5, 7];
h = 1.0;
t = 0.0;
result = ...
    bm.lower((b(2, 1) + b(2, 2) + b(2, 3))*R, h) * ...
    bm.upper((a(2, 1) + a(2, 2) + a(2, 3))*I, h) * ...
    bm.lower((b(1, 1) + b(1, 2) + b(1, 3))*R, h) * ...
    bm.upper((a(1, 1) + a(1, 2) + a(1, 3))*I, h);

assertElementsAlmostEqual( ...
    magnus_splitting(I, R, [t, h], [eye(dim), zeros(dim)], [zeros(dim), eye(dim)], 1, a, b, c), ...
    result);
end

function test_splitting_const_matrix_diff_size
bm = blkmat;
dim = [2, 2];
R = [[1, 2]; [3, 4]];
I = ones(dim);
Z = zeros(dim);
a = [[1, 3, 5]; ...
    [7, 11, 13]];
b = [[17, 19, 23]];
c = [3, 5, 7];
h = 1.0;
t = 0.0;
result = ...
    bm.upper((a(2, 1) + a(2, 2) + a(2, 3))*I, h) * ...
    bm.lower((b(1, 1) + b(1, 2) + b(1, 3))*R, h) * ...
    bm.upper((a(1, 1) + a(1, 2) + a(1, 3))*I, h);

assertElementsAlmostEqual( ...
    magnus_splitting(I, R, [t, h], [eye(dim), zeros(dim)], [zeros(dim), eye(dim)], 1, a, b, c), ...
    result);
end

function test_splitting_vector
bm = blkmat;
dim = [2, 2];
R = [[1, 2]; [3, 4]];
I = ones(dim);
Z = zeros(dim);
a = [[1, 3, 5]; ...
    [7, 11, 13]];
b = [[17, 19, 23]];
c = [3, 5, 7];
h = 1.0;
t = 0.0;
v = rand(2, 1);
w = rand(2, 1);
result = ...
    bm.upper((a(2, 1) + a(2, 2) + a(2, 3))*I, h) * ...
    bm.lower((b(1, 1) + b(1, 2) + b(1, 3))*R, h) * ...
    bm.upper((a(1, 1) + a(1, 2) + a(1, 3))*I, h) * [v; w];

assertElementsAlmostEqual(magnus_splitting(I, R, [t, h], v, w, 1, a, b, c), result);
end
