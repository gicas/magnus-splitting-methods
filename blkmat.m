function bm = blkmat
%   BLKMAT(M,C) returs an upper- or a lower-diagonal block matrix
%   with identity matrices on the main diagonal and M*C above

bm.upper = @blk_up_tri;
bm.lower = @blk_low_tri;
end

function upper = blk_up_tri(M, c)
upper = [[eye(size(M)), c * M]; ...
    [zeros(size(M)), eye(size(M))]];
end

function lower = blk_low_tri(M, c)
lower = [[eye(size(M)), zeros(size(M))]; ...
    [c * M, eye(size(M))]];
end

